.. _gsoc-2017-projects:

:far:`calendar-days` 2017
#########################

BeagleBone AVB Stack
*********************

.. youtube:: 8FDrBt0OAdk
   :width: 100%

| **Summary:** Building a AVB node the stream reservation protocol and the precision time protocol are to be built in the linux kernel. A demo application will be included for a stereo speaker system with two individual beagle boards.

**Contributor:** Indumathi Duraipandian

**Mentors:** Robert Manzke ,Henrik Langer

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2017/projects/4596929535148032
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/BeagleBoneAVB
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

BeagleWire software support
****************************

.. youtube:: Fsj81PMSOC8
   :width: 100%

| **Summary:** The task is to create software support for FPGA cape (based on iCE40 device). The completed project will provide the BeagleBoard.org community with easy to implement and powerful tools for realization of projects based on Programmable Logic Device(FPGA), which will surely increase the number of applications based on it.

**Contributor:** Patryk Mężydło

**Mentors:** Michael Welling ,Jonathan Cameron

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2017/projects/5314472004550656
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/BeagleWire_software_support
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

BeagleLibs: Libraries for interfacing with the BeagleBone in Rust and Go 
**************************************************************************

.. youtube:: svTSmAsZD2I
   :width: 100%

| **Summary:** Well-documented libraries for interfacing with BeagleBone hardware in Rust and Go.

**Contributor:** ee

**Mentors:** Trevor Woerner

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2017/projects/4815942534037504
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/BeagleLibs
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

BeagleBoot , an all in one flasher/updater
******************************************

.. youtube:: 5JYfh2_0x8s
   :width: 100%

| **Summary:** The project is to port the BeagleBone bootloader server BBBlfs(currently written in c) to JavaScript(node.js) and make a cross platform GUI (using electron framework) flashing tool utilising the etcher.io project. 

**Contributor:** Ravi Kumar Prasad

**Mentors:** Jason Kridner

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2017/projects/5066113104740352
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/etcher-beagleboot
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Sonic Anemometer
*****************

.. youtube:: uj8lO8G9QYU
   :width: 100%

| **Summary:** Write program for PRU present in BeagleBoard and to create a portable device able to measure the wind speed and temperature reliably in outdoor environments.

**Contributor:** Naveen Saini

**Mentors:** Stephen Arnold ,Hunyue Yau ,Zubeen Tolani

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2017/projects/6450379722063872
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/GSoC2017_sonic_anemometer_proposal
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

BeagleBone PRU DMA
*******************

.. youtube:: uG6ZjZu9m8E
   :width: 100%

| **Summary:** Most of existing PRU applications utilize (waste) one PRU core for data transfer. The goal of this project is to enable usage of EDMA controller for copying of data to and from main memory (DDR), which would allow applications to use both cores for computation.

**Contributor:** Maciej Sobkowski

**Mentors:** Kumar Abhishek ,Zubeen Tolani

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2017/projects/6614225476648960
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/BeagleBone_PRU_DMA
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip::

   Checkout eLinux page for GSoC 2017 projects `here <https://elinux.org/BeagleBoard/GSoC/2017_Projects>`_ for more details.