.. _gsoc-2023-projects:

:far:`calendar-days` 2023
##########################

Replace GBridge
****************
         
.. youtube:: tCtehnXODW8
   :width: 100%

| **Summary:** The project aims to eliminate GBridge and merge its functionality into greybus (linux driver) and cc1352 driver. The subtasks I have identified are the following:

- Allow Greybus Linux driver to directly communicate with cc1352
- Move SVC and APBridge roles into cc1352
- Eliminate the need for GBridge

**Contributor:** Ayush Singh

**Mentors:** Jason Kridner, Vaishnav Achath

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2023/projects/iTfGBkDk
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2023_Proposal/AyushSingh
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal


Building an LLVM Backend for PRU
*********************************

.. youtube:: f4LVIW9VlrM
  :width: 100%

| **Summary:** This project intends to introduce LLVM support for PRU, to enable us to use clang rather than pru-gcc. As Clang provides extremely clear and simple diagnoses and is considerably faster and requires less memory, it will be beneficial. The LLVM support will offer greater tooling, compatibility, and optimization. I will therefore build the LLVM Backend for PRU.

**Contributor:** Khushi Balia

**Mentors:** Vedant Paranjape, Shreyas Atre, Abhishek Kumar

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2023/projects/nfmrQ9LL 
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2023_Proposal/Khushi-Balia
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Zephyr on R5/M4F
*****************
         
.. youtube:: zGILLbQjVRI
  :width: 100%

| **Summary:** This project focuses on optimizing the TDA4VM SoC by integrating Zephyr RTOS support for the Cortex-R5 processor core. Leveraging Zephyr's versatility, the goal is to seamlessly run it alongside Linux on the Cortex-A72 core through remoteproc. Peripheral support enhancements include Interrupts, GPIO, UART, and Timers. The Cortex-R5's design for real-time and safety-critical systems emphasizes improved user experience and system functionality. Programming languages include Assembly and C, aligning with industry standards. This initiative aims to deliver a robust software infrastructure, contributing to advancements in embedded systems development.

**Contributor:** Prashanth S

**Mentors:** Nishanth Menon, Dhruva Gole, Abhishek Kumar, Vaishnav Achath, Deepak Khatri

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2023/projects/MyuaawJq 
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2023_Proposal/PrashanthS
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

OpenGLES Acceleration for DL
*****************************

.. youtube:: jSTgFrK3ZOw
  :width: 100%

| **Summary:** The primary objective of the initiative is to optimize the speed of diverse layer types through the integration of OpenGLES and Darknet within deep learning frameworks. Enhancing the performance of deep learning models is imperative for real-time applications, and GPUs serve as a pivotal tool for accelerating computations by leveraging parallel processing capabilities. OpenGLES, a prevalent graphics API, becomes instrumental in harnessing the GPU's parallel processing prowess, ensuring a significant boost in the overall performance of deep learning models.

**Contributor:** Pratham Deskmukh

**Mentors:** Shreyas Atre, Deepak Khatri

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2023/projects/gf9eB3q9 
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2023_Proposal/OpenGLES_acceleration_for_DL
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip:: 
   
   Checkout eLinux page for GSoC 2023 projects `here <https://elinux.org/BeagleBoard/GSoC/2023_Projects>`_ for more details.