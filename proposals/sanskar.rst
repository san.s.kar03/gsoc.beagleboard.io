.. _gsoc-proposal-template:

Proposal  
#################

Introduction
*************
My name is Sanskar Jain. 
I am currently pursuing my B.Tech in Electronics and Communication Engineering from Jabalpur Engineering College.
I am currently in my 3rd year of my bachelor's. 
**Project:** `Low-latency I/O RISC-V CPU core in FPGA fabric <https://gsoc.beagleboard.io/ideas/>`_
I have already worked in verilog projects.

Summary links
=============

- **Contributor:** `Sanskar Jain <https://forum.beagleboard.org/u/san.s.kar03>`_
- **Mentors:** `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_, `Cyril Jean <https://forum.beagleboard.org/u/vauban>`_
- **Code:** `Google Summer of Code / greybus / cc1352-firmware · GitLab <https://openbeagle.org/san.s.kar03/gsoc.beagleboard.io>`_
- **Documentation:** `Sanskar Jain/ docs.beagleboard.io · GitLab <https://openbeagle.org/san.s.kar03/docs.beagleboard.io>`_
- **GSoC:** `Google Summer of Code <https://summerofcode.withgoogle.com/programs/2024/organizations/beagleboardorg>`_ 

Status
=======

This project is currently just a proposal.

Proposal
========

I have completed the task given.

About 
=====

- **Forum:** :fab:`discourse` `u/san.s.kar03 (Sanskar Jain) <https://forum.beagleboard.org/u/san.s.kar03>`_
- **OpenBeagle:** :fab:`gitlab` `san.s.kar03 (Sanskar Jain) <https://openbeagle.org/san.s.kar03>`_
- **IRC:** :fas:`comments` `sanskar (Sanskar Jain) <https://web.libera.chat/gamja/#beagle>`_
- **Github:** :fab:`github` `SanskarJain1009 (Jason Kridner) <https://github.com/SanskarJain1009>`_
- **School:** :fas:`school` Jabalpur Engineering College
- **Country:** :fas:`flag` India
- **Primary language:** :fas:`language` English Hindi
- **Typical work hours:** :fas:`clock` 9AM-3PM India Standard Time (IST)
- **Previous GSoC participation:** :fab:`google` N/A

Project
********

**Project name:** Astrotinker Bot

Description
============

Astrotinker Bot— Details

Made this bot for the competition of IIT Bombay named eyantra.

This bot contains LFA sensor, Ultrasonic Sensor, HC-05 Bluetooth and Electromagnetic. All modules are connected together for serial communication and parallel processing. 

Modules-UART Protocol for BLE, PWM and frequency scaling for Ultrasonic and for different clocks needed.

Implemented the instruction present in RISC V ISA for running program (that is converted to hex file). 

This contains 512 registers of 32 bits for instruction memory and 64 registers of 32 bits for data memory.  

`Video <https://www.youtube.com/watch?v=HmsYA7icHxA>`_ We didn’t qualified for the finale but this video 
demonstrates the basic working of this bot (this video was made for the submission during this competition).

Software
=========

Quartus Prime Lite Ediition, ModelSim Altera, Verilog, C (C++ if needed) and any tech stack needed to complete this project in future.

Hardware
========

DE0 Nano FPGA.

Timeline
********

Provide a development timeline with 10 milestones, one for each week of development without 
an evaluation, and any pre-work. (A realistic, measurable timeline is critical to our selection process.)

.. note:: This timeline is based on the `official GSoC timeline <https://developers.google.com/open-source/gsoc/timeline>`_


Timeline summary
=================

.. table:: 

    +------------------------+----------------------------------------------------------------------------------------------------+
    | Date                   | Activity                                                                                           |                                  
    +========================+====================================================================================================+
    | February 26            | Connect with possible mentors and request review on first draft                                    |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 4                | Complete prerequisites, verify value to community and request review on second draft               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 11               | Finalized timeline and request review on final draft                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 21               | Submit application                                                                                 |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 1                  | Start bonding                                                                                      |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 27                 | Start coding and introductory video                                                                |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 3                 | Release introductory video and complete milestone #1                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 10                | Complete milestone #2                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 17                | Complete milestone #3                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 24                | Complete milestone #4                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 1                 | Complete milestone #5                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 8                 | Submit midterm evaluations                                                                         |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 15                | Complete milestone #6                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 22                | Complete milestone #7                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 29                | Complete milestone #8                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 5               | Complete milestone #9                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 12              | Complete milestone #10                                                                             |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 19              | Submit final project video, submit final work to GSoC site and complete final mentor evaluation    |
    +------------------------+----------------------------------------------------------------------------------------------------+

Timeline detailed
=================

Community Bonding Period (May 1st - May 26th)
==============================================

GSoC contributors get to know mentors, read documentation, get up to speed to begin working on their projects

Coding begins (May 27th)
=========================

Milestone #1, Introductory YouTube video (June 3rd)
====================================================

Milestone #2, ISA analysis of RISC V with application/working of interupts (June 10th)
=======================================================================================

Milestone #3 Environment Setup for Development (June 17th)
===========================================================

Milestone #4 Module Initialisation from lower most to top entity module (June 24th)
====================================================================================

Milestone #5 Creating another top level entity to verify the working of RISC V by creating external modules i.e. RAM, external inputs (to verify interupts) (July 1st)
=======================================================================================================================================================================

Submit midterm evaluations (July 8th)
======================================

.. important:: 
    
    **July 12 - 18:00 UTC:** Midterm evaluation deadline (standard coding period) 

Milestone #6 Completion of the remaining Logic Building  (July 15th)
=====================================================================

Milestone #7 TestBench making and analysis (July 22nd)
=======================================================

Milestone #8 Error Handling and more TestBench making and analysis (July 29th)
===============================================================================

Milestone #9 Creating codes in C and then converting them to hex file and then analyzing the memory lists in ModelSim Altera to reduce the latency.(Aug 5th)
=============================================================================================================================================================

Milestone #10 Verifying the design, testing for multiple codes and testing different I/O (Aug 12th)
====================================================================================================

Final YouTube video (Aug 19th)
===============================

Submit final project video, submit final work to GSoC site 
and complete final mentor evaluation

Final Submission (Aug 24nd)
============================

.. important::

    **August 19 - 26 - 18:00 UTC:** Final week: GSoC contributors submit their final work 
    product and their final mentor evaluation (standard coding period)

    **August 26 - September 2 - 18:00 UTC:** Mentors submit final GSoC contributor 
    evaluations (standard coding period)

Initial results (September 3)
=============================

.. important:: 
    **September 3 - November 4:** GSoC contributors with extended timelines continue coding

    **November 4 - 18:00 UTC:** Final date for all GSoC contributors to submit their final work product and final evaluation

    **November 11 - 18:00 UTC:** Final date for mentors to submit evaluations for GSoC contributor projects with extended deadline

Experience and approach
***********************

I have experience in writing code in verilog.
I already made a RISC V R32I CPU architecture which is fully functional.
I also integrated my RISC V CPU with instruction memory(word aligned) and data memory(byte aligned) to execute my programs.
I created 3 testbenches for error analysis. 
These 3 testbenches I created for verifying my C programs (which converted to hex for execution).
Verified programs are Dijkstra’s Algorithm, Arithmetic progression for n numbers and sum of n numbers.
I am telling you all this because while doing this project I encountered various errors and since I had no mentor I had to resolve those problems. 
At this moment I am confident in my debugging abilities.
I don't remember exactly but I spent hours without break many times to analyze my codes and to debug errors.
Like in Dijkstra’s Algorithm, I had to debug in ModelSim Altera (RTL Simulation) for verifying every instruction executed.
I know the basic of implementing any CPU if I am given its ISA. 
In this project my approach will be same as I mentioned in the Timeline. I am confident in completing the tasks that I mentioned in it.   

Contingency
===========

I will analyze my code few times. Then I will ask in forum and at the same time I will ask my friends also.

Suggestions
===========

I think you should have asked about my professional experiences, interests and my passion for this project.
